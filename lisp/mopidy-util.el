;; Utility and helper functions

;;(defun mopidy--helm-get-attribute-name (token)
;;  "Get an attribute name from TOKEN."
;;  (cond
;;   ((= token "a") "ALBUM")
;;   ((= token "A") "ARTIST")
;;   ((= token "d") "DIRECTORY")
;;   ((= token "p") "PLAYLIST")
;;   ((= token "T") "PLAYLIST")
;;   (t (message "Unknown key."))
;;   ))

;; TODO Get list of attribs from alist
(defun mopidy--get-attributes-from-alist (alist keys)
  "Return a list consisting of the KEYS in the ALIST."
  (let ((result-list nil))
    (dolist (key (reverse keys))
      (push (cdr (assoc key alist)) result-list))
    result-list))

(defun mopidy--get-icon-or-string (icon)
  "Return the icon or the string itself."
  (cond
   ((symbolp icon)
    (icons-in-terminal icon))
   (t icon)))

;; TODO Proper rendering + Settings for rendering
(defun mopidy--render-track (track &optional playing)
  "Generate a string representation of the track.
PLAYING is t if the track should be shown as playing."
  (let* ((playing-icon (mopidy--get-icon-or-string mopidy-icon-play))
         (line-start (if playing
                         playing-icon
                       (string-join (make-list (length playing) " ")))))
    (format "%s %s %s - %s\t\t%i:%02i"
            line-start
            (mopidy--get-icon-or-string mopidy-icon-lighter)
            (cdr (assoc 'name track))
            (mapconcat (lambda (artist) (cdr (assoc 'name artist))) (cdr (assoc 'artists track)) ", ")
            (/ (/ (cdr (assoc 'length track)) 1000) 60)
            (mod (/ (cdr (assoc 'length track)) 1000) 60)
            )))

(provide 'mopidy-util)
