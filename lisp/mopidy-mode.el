;;; Basis for integrating mopidy into helm / emacs
;; Todos:
;; * Current track in mode line
;; * Search for tracks / playlists / ...
;; * Prev / Next Track
;; * Playback mode (stopped, pause, running)
;; * Error handling: what if connection is aborted?
;; * Register for signals
;; * Select next song for play back
;; * Build requests from alist not from string
;; * Custom display for songs
;; * Tracklist beautification
;; * Key map
;; Bugs:
;; * Id is set to nil

;; Used Packages / Resources
;; * mopidy: https://docs.mopidy.com/en/latest/api/http/
;; * mopidy-gmusic: https://github.com/mopidy/mopidy-gmusic
;; * emacs-websocket: https://github.com/ahyatt/emacs-websocket
;; * helm: http://wikemacs.org/wiki/How_to_write_helm_extensions
;; * json.el https://github.com/thorstadt/json.el
;; * json rpc2 http://www.jsonrpc.org/specification#request_object

(unless (require 'icons-in-terminal nil t)
  (defun icons-in-terminal (&rest _)
    " "))

(require 'mopidy-helm)
(require 'mopidy-util)
(require 'mopidy-websocket)

;; Icons
(defvar mopidy-icon-lighter 'fa_music "Lighter icon, string or icons-in-terminal symbol")
(defvar mopidy-icon-play 'fa_play "Icon for playback, string or icons-in-terminal symbol.")
(defvar mopidy-icon-pause 'fa_pause "Icon for pause, string or icons-in-terminal symbol")
(defvar mopidy-icon-stop 'fa_stop "Icon for stop, string or icons-in-terminal symbol")
(defvar mopidy-icon-next 'fa_forward "Icon for next song, string or icons-in-terminal symbol")
(defvar mopidy-icon-prev 'fa_backward "Icon for previous song, string or icons-in-terminal symbol")
(defvar mopidy-icon-shuffle 'md_shuffle "Icon for shuffle, string or icons-in-terminal symbol")
(defvar mopidy-icon-repeat 'fa_repeat "Icon for shuffle, string or icons-in-terminal symbol")

(setq mopidy-icon-lighter 'fa_music)
(setq mopidy-icon-play 'fa_play)
(setq mopidy-icon-pause 'fa_pause)
(setq mopidy-icon-stop 'fa_stop)
(setq mopidy-icon-next 'fa_forward)
(setq mopidy-icon-prev 'fa_backward)
(setq mopidy-icon-shuffle 'md_shuffle)
(setq mopidy-icon-repeat 'fa_repeat)

;; Debugging Settings
(defvar mopidy-debug-echo-message nil "If non nil, all received messages from the mopidy server are printed.")

;; Connection Settings
(defvar mopidy-host "localhost" "Host the mopidy server is running on")
(defvar mopidy-port 6680 "Port of the mopidy server")
(defvar mopidy-wait-duration 0.2 "Timespan to wait for answers from mopidy server.")

(defun mopidy-play ()
  "Play the current song"
  (interactive)
  (mopidy--play))

(defun mopidy-next-song ()
  "Change to the next track"
  (interactive)
  (mopidy--next-song))

(defun mopidy-next-song ()
  "Change to the next track"
  (interactive)
  (mopidy--next-song))

(defun mopidy-previous-song ()
  "Change to the previous track"
  (interactive)
  (mopidy--previous-song))

(defun mopidy-stop ()
  "Stop playing"
  (interactive)
  (mopidy--stop))

(defun mopidy-pause ()
  "Pause playing"
  (interactive)
  (mopidy--pause))

(defun mopidy-resume ()
  "Resume playback"
  (interactive)
  (mopidy--resume))

(defun mopidy-seek (time_position)
  "Seeks to TIME_POSITION given in milliseconds."
  (interactive)
  (mopidy--resume))

;;;###autoload
(define-minor-mode mopidy-mode "Control mopidy from emacs."
  :lighter (mopidy--get-icon-or-string 'mopidy-icon-lighter)
  :global t
  (setq mopidy--connection (mopidy--connect-to-mopidy)))

(provide 'mopidy-mode)
