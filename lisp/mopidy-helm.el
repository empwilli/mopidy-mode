;; Any helm related functions

(require 'mopidy-websocket)
(require 'mopidy-util)

(defun mopidy--helm-render-track (track)
  "Generate a suitable entry for helm, consisting of a string and the id/uri."
  (let ((current-track (mopidy--get-current-track))
        (id (if (assoc 'tlid track)
                (cdr (assoc 'tlid track))
              (cdr (assoc 'uri track))))
        (track (if (assoc 'track track)
                   (cdr (assoc 'track track))
                 track)))
    (cons (mopidy--render-track track (equal id current-track)) id)))

;; TODO: How to handle nesting: Albums, Artists, URIs.
;; TODO: Strange bug were the search result is nil, maybe increase waiting time? Block until not ready?
(defun mopidy--helm-search (query)
  "Return all songs matching QUERY"
  (let* ((results (aref (mopidy--search query) 1))
         (search-results (cdr (assoc 'tracks results)))) ;; Workaround until it is clear how to handle multiple uris
    (mapcar 'mopidy--helm-render-track search-results)))

(defun mopidy--helm-search-tracks (query)
  `((name . "Playlist")
    (candidates . ,(mopidy--helm-search query))
    (action . (lambda (candidate)
                (mopidy--play (cdr (assoc 'tlid (aref
                                                 (mopidy--add-uri candidate) 0))))))))

(defun helm-mopidy-search (query)
  "Search for tracks matching QUERY"
  (interactive "sEnter search query: ")
  (insert
   (mapconcat 'identity
              (helm :sources (mopidy--helm-search-tracks query))
              ",")))

(defun mopidy--helm-get-tracklist ()
  "Get the playlist as string"
  (let ((tracklist (mopidy--get-tracklist)))
    (mapcar 'mopidy--helm-render-track tracklist)))

(defun mopidy--helm-tracklist ()
      `((name . "Playlist")
        (candidates . ,(mopidy--helm-get-tracklist))
        (action . (lambda (candidate)
                    (mopidy--play candidate)))))

(defun helm-mopidy-tracklist ()
  (interactive)
  (insert
   (mapconcat 'identity
              (helm :sources (mopidy--helm-tracklist))
              ",")))

(provide 'mopidy-helm)
