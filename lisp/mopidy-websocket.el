;; Communication with mopidy server

(require 'websocket)
(eval-when-compile (require 'cl))
(require 'json)

;; Player Status
(defvar mopidy--connection nil "Connection to the mopidy server")
(defvar mopidy--answer nil "Holds the most recent answer of the mopidy server") ;; To be removed
(defvar mopidy--playlist nil "Current playlist")
(defvar mopidy--tracklist nil "Current tracklist")
(defvar mopidy--stream-title nil "Current track")
(defvar mopidy--current-track -1 "Current track") ;; -1 is unset, no current track is nil
(defvar mopidy--playback nil "Stores the current playback state (running, paused, finished).")
(defvar mopidy--muted -1 "Stores whether the server is currently muted.") ;; -1 is unset, no current track is nil
(defvar mopidy--random -1 "Stores whether the server is muted.") ;; -1 is unset, no current track is nil
(defvar mopidy--repeat -1 "Stores whether the server is in repeat mode.") ;; -1 is unset, no current track is nil
(defvar mopidy--single -1 "Stores whether the server is in single mode.") ;; -1 is unset, no current track is nil
(defvar mopidy--consume -1 "Stores whether the server is in consume mode.") ;; -1 is unset, no current track is nil
(defvar mopidy--volume nil "Stores whether the server is in consume mode.")
(defvar mopidy--time-position 0 "Stores progress of the current track.")

;; Mopidy functions - Working with the mopidy server
(defun mopidy--event-mute-changed (mute)
  "Called when mute option is changed.
MUTE contains the new mute value"
  (setq mopidy--muted mute))

(defun mopidy--event-option-changed ()
  "Called when an option is changed.")

(defun mopidy--event-playback-state-changed (old_state new_state)
  "Called when the playback state changes.
The state changed from OLD_STATE to NEW_STATE."
  (setq mopidy--playback new_state))

(defun mopidy--event-playlist-changed (playlist)
  "Called when the playlist changes.
PLAYLIST is the new playlist."
  (setq mopidy--playlist playlist))

(defun mopidy--event-playlist-delete (uri)
  "Called when a playlist is deleted.
URI of the deleted playlist.")

(defun mopidy--event-playlists-loaded ()
  "Called when playlists are loaded.")

(defun mopidy--event-seeked (time_position)
  "Called when the time changes significantly.
TIME_POSITION contains the new position"
  (setq mopidy--time-position time_position))

(defun mopidy--event-stream-title-changed (title)
  "Called when the current stream title changes.
The new title is in TITLE.")

(defun mopidy--event-track-playback-ended (tl_track time_position)
  "Called when playback of the track ended.
The id of the track is in TL_TRACK, the time of end is TIME_POSITION.")

(defun mopidy--event-track-playback-paused (tl_track time_position)
  "Called when playback of the track paused.
The id of the track is in TL_TRACK, the time of end is TIME_POSITION.")

(defun mopidy--event-track-playback-resumed (tl_track time_position)
  "Called when playback of the track resumed.
The id of the track is in TL_TRACK, the time of end is TIME_POSITION.")

(defun mopidy--event-track-playback-started (tl_track time_position)
  "Called when playback of the track started.
The id of the track is in TL_TRACK, the time of end is TIME_POSITION."
       (setq mopidy--tracklist (mopidy--get-current-track)))

(defun mopidy--event-tracklist-changed ()
  "Called when the tracklist changes."
  (setq mopidy--tracklist (mopidy--get-tracklist)))

(defun mopidy--event-volume-changed (volume)
  "Called when the volume changes.
The new VOLUME is given as  an int 1-100"
  (setq mopidy--volume volume))

(defconst mopidy--events '(("mute_changed" . '(mopidy--event-mute-changed . '(mute)))
                           ("option_changed" . 'mopidy--event-option-changed)
                           ("playback_state_changed" . '(mopidy--event-playback-state-changed . '(old_state new_state)))
                           ("playlist_changed" . '(mopidy--event-playlist-changed . '(playlist)))
                           ("playlist_delete" . '(mopidy--event-playlist-delete . '(playlist)))
                           ("playlists_loaded" . mopidy--event-playlists-loaded)
                           ("seeked" . '(mopidy--event-seeked . '(time_position)))
                           ("stream_title_changed" . '(mopidy--event-stream-title-changed . '(title)))
                           ("track_playback_ended" . '(mopidy--event-track-playback-ended . '(tl_track time_position)))
                           ("track_playback_paused" . '(mopidy--event-track-playback-paused . '(tl_track time_position)))
                           ("track_playback_resumed" . '(mopidy--event-track-playback-resumed . '(tl_track time_position)))
                           ("track_playback_started" . '(mopidy--event-track-playback-started . '(tl_track)))
                           ("tracklist_changed" . mopidy--event-tracklist-changed)
                           ("volume_changed" . '(mopidy--event-volume-changed . '(volume))))
  "Callbacks for message events")

;; TODO How to map the args?
;;(defun mopidy--handle-message (frame))

;; TODO: Do stuff depending on refceived message type
(defun mopidy--on-message-callback-function (_websocket frame)
  "Process the contents of a message.
A message can be one of two categories:
* An event: The server notifies about some changes, which have to be taken care of
* An answer to a command sent to the server.

In case of an event, the appropriate event handler is called (see
mopidy-events).

In case the message is an answer to a previous request, this
function tries to be as smart as possible to identify what
request that was and to store the results accordingly.
"
  (let ((response (json-read-from-string (websocket-frame-text frame))))
    (cond
     ((assoc 'error response)
      (error "%s: %s"
             (cdr (assoc 'message (cdr (assoc 'error response))))
             (cdr (assoc 'data (cdr (assoc 'error response))))))
     ((assoc 'event response) ;; events always have an "event" key
      ;; Get event type and corresponding handler
      ;; A handler is either a function or a cons of a function and the keys
      ;; for the attribute names in the message.
      (let* ((event-type (cdr (assoc 'event response)))
             (event-handler (cdr (assoc event-type mopidy--events))))
        (message "Responding to event: %S %S" event-type event-handler)
        (unless event-handler
          (error "Received unrecognized event type: %s" event-type))
        (if (functionp event-handler)
            (funcall event-handler)
          (apply (car event-handler)
                 (mopidy--get-attributes-from-alist (cdr event-handler))))))
     (t (setq mopidy--answer
              (cdr (assoc 'result (json-read-from-string (websocket-frame-text frame)))))))))

;; TODO: Reset variables on close / error, trigger reconnect on failure
(defun mopidy--connect-to-mopidy ()
  "Create and return a web socket connection to the mopidy server.
Reads the mopidy-host and mopidy-port variables"
  (websocket-open (format "ws://%s:%i/mopidy/ws" mopidy-host mopidy-port)
                  :on-open (lambda (_websocket)
                             (message "Connected to mopidy server."))
                  :on-message 'mopidy--on-message-callback-function
                  :on-close (lambda (_websocket)
                              (message "Connection to mopidy server closed."))
                  :on-error (lambda (_websocketerror source error)
                              (message "An error happened in the server: %s" (cdr error)))
                  ))

;; Helpers
(defun mopidy--await-value (symbol &optional unset attempts)
  "Await for symbol to be set, maximum ATTEMPTS times.
Waits for mopidy--wait-duration. Per default, wait up to 5
times."
  (unless attempts
    (setq attempts 5))
  (let ((value (symbol-value symbol))
        (iter 0))
    (while (and (equal value unset)
                (< iter attempts)
                (websocket-openp mopidy--connection))
      (sleep-for mopidy-wait-duration)
      (setq value (symbol-value symbol))
      (setq iter (1+ iter))
      )
    (unless (not (equal value unset))
      (error "Waiting for %S timed out" symbol))
    value))

;; Requests
(defun mopidy--get-tracklist ()
  "Get the current playlist.
Returns the current playlist as an alist, as described here
https://docs.mopidy.com/en/latest/api/core/#playlists-controller" ;
  (unless mopidy--tracklist
    (setq mopidy--answer nil)
    (websocket-send-text mopidy--connection "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.tracklist.get_tl_tracks\"}")
    (mopidy--await-value 'mopidy--answer)
    (setq mopidy--tracklist mopidy--answer))
  mopidy--tracklist
  )

(defun mopidy--search (query)
  "Search for tracks matching-paren QUERY.
Returns the current playlist as an alist, as described here
https://docs.mopidy.com/en/latest/api/core/#playlists-controller"
  (setq mopidy--answer nil)
  (websocket-send-text mopidy--connection (format "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.library.search\", \"params\": {\"any\": [\"%s\"]}}" query))
  (mopidy--await-value 'mopidy--answer)
  mopidy--answer
  )

(defun mopidy--get-current-track ()
  "Get the current playlist.
Returns the current playlist as an alist, as described here
https://docs.mopidy.com/en/latest/api/core/#playlists-controller"
  (unless (not (equal -1 mopidy--current-track))
    (setq mopidy--answer -1)
    (websocket-send-text mopidy--connection "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.playback.get_current_tl_track\"}")
    (mopidy--await-value 'mopidy--answer)
  (setq mopidy--current-track mopidy--answer))
  mopidy--current-track)

(defun mopidy--add-uri (uri)
  "Add the track with ID to the tracklist"
  (setq mopidy--answer nil)
  (websocket-send-text mopidy--connection (format "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.tracklist.add\", \"params\": {\"uri\": \"%s\"}}" uri))
  (mopidy--await-value 'mopidy--answer)
  mopidy--answer
  )

(defun mopidy--play (&optional tlid)
  "Play the track with tracklist id TLID from tracklist.
If TLID is nil, play the current song"
  (cond (tlid (websocket-send-text mopidy--connection (format "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.playback.play\", \"params\": {\"tlid\": %i}}" tlid)))
        (t (websocket-send-text mopidy--connection (format "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.playback.play\"}")))))

(defun mopidy--next-song ()
  "Change to the next track"
  (websocket-send-text mopidy--connection (format "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.playback.next\"}"))
  )

(defun mopidy--previous-song ()
  "Change to the previous track"
  (websocket-send-text mopidy--connection (format "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.playback.previous\"}"))
  )

(defun mopidy--stop ()
  "Stop playing"
  (websocket-send-text mopidy--connection (format "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.playback.stop\"}"))
  )

(defun mopidy--pause ()
  "Pause playing"
  (websocket-send-text mopidy--connection (format "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.playback.pause\"}"))
  )

(defun mopidy--resume ()
  "Pause playing"
  (websocket-send-text mopidy--connection (format "{\"jsonrpc\": \"2.0\", \"id\": 1, \"method\": \"core.playback.resume\"}"))
  )

(provide 'mopidy-websocket)
