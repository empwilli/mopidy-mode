# mopidy.el

Package for controlling Mopidy from Emacs using the JSON-RPC API. Simple helm
integration included.

## Getting Started

Get your own copy of this package somewhere:

```
git clone https://gitlab.com/empwilli/mopidy-mode.git
```

Edit your `init.el` so that the package is loaded:

```
(add-to-list 'load-path "~/coding/mopidy-mode/lisp/")
(require 'mopidy-mode)
```

You now can control the playback in Mopidy using the
`mopidy-{play, pause, stop, resume, next-song, previous-song}` functions.

In order to see the current playlist, you can use `helm-mopidy-tracklist`.

The `helm-mopidy-search` function allows you to search for tracks in your
library and to add them to the current tracklist.

### Prerequisites

This package relies on a couple of other packages, please make sure to install
them in order for the package to work correctly.

* [icons-in-terminal](https://github.com/sebastiencs/icons-in-terminal)
* [emacs-websocket](https://github.com/ahyatt/emacs-websocket)
* [json.el](https://github.com/thorstadt/json.el)

All code has (only) been tested in emacs 26.1, so I note this as a requirement,
as well.

## Configuration

There are some options which influence the way mopidy-mode works:

* The `mopidy-icon-*` variables configure icons which are displayed. The option
  accepts (utf8) characters or `icons-in-terminal` symbols
* `mopidy-debug-echo-message` is for debugging only and displays events received
  from the mopidy server
* `mopdiy-host` and `mopidy-port` specify the connection details for the mopidy
  mpd server
* `mopdiy-wait-duration` The duration mopidy-mode waits for replies from the
  mopidy server

## Using this package

Please take note, that this package is in a very early state and that there will
be a lot of development going on. I'd like to make sure you are aware of this
packages short-comings:

### Why should I use this package

* As far as I know there is currently no package providing integration of Mopidy
  and helm

### Why should I not use this package

* The main motivation for this package is to learn elisp, that being said: I try
  my best but can't promise that everything is done in a way "proper" elisp
  packages should be written in
* Currently, there is only a fraction of the functionality of a proper Mopdiy
  client available
* No tests, whatsoever (this changes hopefully soon)

## Planned Features

* Extension of tracklist functions
* Extension of search functions
* Playback indicator for mode line
* Configurable visualization of tracks in search/tracklist

## Built With

* [icons-in-terminal](https://github.com/sebastiencs/icons-in-terminal) - Fancy
  icons for displaying the player status.
* [emacs-websocket](https://github.com/ahyatt/emacs-websocket) - Performing web
  socket requests
* [json.el](https://github.com/thorstadt/json.el) - For unpacking JSON responses
  of the server

## Contributing

This is project is in a very early stage, so heavy changes are (hopefully) to be
expected. Nevertheless, you are welcome to contribute by:

* Reporting Bugs
* Feature Requests
* Pull Requests

For the latter, please write me an email in advance, containing any
changes/fixes.

## Licence

mopidy-mode is released under MIT licence (at least for the moment) — see
[LICENCE.md](LICENCE.md) for details.

## Authors

* **Tobias Langer** - *Initial work*

## Acknowledgments

* Hat tip to anyone whose code was used
